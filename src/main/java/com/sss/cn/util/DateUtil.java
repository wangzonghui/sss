package com.sss.cn.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
/**
 * 时间帮助
 * @description 
 * @author Dylan Tao
 * @date 2015-11-12
 * Copyright 2015 bigaka.com. All Rights Reserved.
 */

public class DateUtil {

	private static final long ONE_DAY_LIMIT = 86400000;
	
	private static SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static SimpleDateFormat longformat = new SimpleDateFormat("yyyyMMddHHmmss");
	
	private static SimpleDateFormat wechatFormat = new SimpleDateFormat("yyyy年MM月dd日");
	
	/**
	 * 获取当前时间
	 * @return
	 */
	public static long getTime(){
		return Long.parseLong(longformat.format(new Date()));
	}
	
	/**
	 * 时间格式 XXXX年XX月XX日
	 * @return
	 */
	public static String getWechatTime(){
		return wechatFormat.format(new Date());
	}
	public static long getTime(Date date){
		try {
			return Long.parseLong(longformat.format(date));
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
	public static String getDay(int amount){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, amount);
		return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	}
	
	//5分钟
	public static long getFiveMinutesLimit(){
		long time = System.currentTimeMillis() - 300000;
		return Long.parseLong(longformat.format(new Date(time)));
	}

	public static long getTodayStartTime(Date date) {
		Calendar todayStart = Calendar.getInstance();
		todayStart.set(Calendar.HOUR_OF_DAY, 0);
		todayStart.set(Calendar.MINUTE, 0);
		todayStart.set(Calendar.SECOND, 0);
		todayStart.set(Calendar.MILLISECOND, 0);
		return todayStart.getTime().getTime();
	}

	public static long getTodayPlusOneDay(Date date) {
		return getTodayStartTime(date) + ONE_DAY_LIMIT;
	}

	public static long getTodayPlusTwoDay(Date date) {
		return getTodayStartTime(date) + ONE_DAY_LIMIT * 2;
	}

	public static int getHour(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}
	
	public static Long getMinDate(String date){
		try {
			return Long.parseLong(longformat.format(dateformat.parse(date+" 00:00:00")));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Long getMaxDate(String date){
		try {
			return Long.parseLong(longformat.format(dateformat.parse(date+" 23:59:59")));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	// el ref
	public static String formatDate(Long time) {
		try {
			return dateformat.format(longformat.parse(time+""));
		} catch (ParseException e) {
//			e.printStackTrace();
			System.out.println("jsp页面格式化时间异常!");
		}
		return null;
	}
	
	public static int formatAge(Long birthday) {
        Calendar calendar = GregorianCalendar.getInstance();
        try {
            calendar.setTime(new Date());
            int theYear=calendar.get(Calendar.YEAR);
            calendar.setTime(dateformat.parse(formatDate(birthday)));
            int birthYear=calendar.get(Calendar.YEAR);
            int year=theYear-birthYear;
            return year;
        } catch (ParseException e) {
            System.out.println(e);
        }
        return 0;
    }
	
	public static String getTime(int date) {
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(new Date());
		cal.add(Calendar.DATE, date);
		  
	    String yesterday = new SimpleDateFormat( "yyyy-MM-dd").format(cal.getTime());
		
		return yesterday;
	}

}
