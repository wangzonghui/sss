package com.sss.cn.util;

/**
 * 异常管理类
 * @description 
 * @author Dylan Tao
 * @date 2015-9-6
 * Copyright 2015 bigaka.com. All Rights Reserved.
 */
public class ServiceException extends java.lang.RuntimeException{


	private static final long serialVersionUID = 1L;
	
	 private Long errorCode; 
	 
	 public ServiceException(Long errorCode, String message) {
         this(errorCode, message, null);
	 }
	 
     public ServiceException(Long errorCode, Throwable cause) {
         this(errorCode, null, cause);
     }
     
     public ServiceException(Long errorCode, String message, Throwable cause) {
            super(message, cause);
            this.errorCode = errorCode;
     }
     public Long getErrorCode() {
            return errorCode;
     } 

}
