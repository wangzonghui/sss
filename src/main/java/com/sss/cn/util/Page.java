package com.sss.cn.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页对象
 * @description 
 * @author Dylan Tao
 * @date 2015-11-12
 * Copyright 2015 bigaka.com. All Rights Reserved.
 */

public class Page<T> implements Serializable {
	
	private static final long serialVersionUID = 9048608050264035103L;
	private static final int defaultPageNo = 1;
	private static final int defaultPageSize = 12;
	
	protected int pageNo;
	protected int pageSize;
	protected long totalCount = 0;
	//totalPage会自动计算显示
	
	protected boolean autoCount = true; //是否统计总数量
	
	protected List<T> result =  new ArrayList<T>();
	
	public Page(int pageSize, int pageNo) {
		if (pageSize > 0) {
			this.pageSize = pageSize;
		}else{
			this.pageSize = defaultPageSize;
		}
		if (pageNo > 0) {
			this.pageNo = pageNo;
		} else {
			this.pageNo = defaultPageNo;
		}
	}

	public void setPageSize(int pageSize) {
		if (pageSize > 0) {
			this.pageSize = pageSize;
		}else{
			this.pageSize = defaultPageSize;
		}
	}
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageNo(int pageNo) {
		if (pageNo > 0) {
			this.pageNo = pageNo;
		} else{
			this.pageNo = defaultPageNo;
		}
	}
	
	public int getPageNo() {
		return pageNo;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}
	
	public List<T> getResult() {
		return result;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(final long totalCount) {
		this.totalCount = totalCount;
	}

	public int getTotalPage() {
		if (totalCount < 0) {
			return 0;
		}else{
			int count = (int)((totalCount + pageSize - 1) / pageSize);
			return count;
		}
	}

	public boolean isAutoCount() {
		return autoCount;
	}

	public void setAutoCount(boolean autoCount) {
		this.autoCount = autoCount;
	}
}
