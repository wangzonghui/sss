package com.sss.cn.util;

/**
 * 通用常量类
 * @description 
 * @author Dylan Tao
 * @date 2015-11-12
 * Copyright 2015 bigaka.com. All Rights Reserved.
 */
public interface Constant {

	String CLASS = "class";
	String DOUBLE_LINE = "//";
	String DOT = ".";
	String LINE = "/";
	String COLON = ":";
	String COMMA = ",";
	String EMPTY = "";
	String SPACE = " ";
	String HR = "-";
	String QM = "?";
	String EM = "=";
	String AM = "&";

	// 排序规范
	public enum OrderConstant{
		DESC, ASC
	}
	
	// 图片格式
	public enum ImgType {
		
		JPG("jpg"), JPEG("jpeg"), PNG("png"), GIF("gif"), BMP("bmp");

		private final String value;

		ImgType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

	}
	
	// 计算符号
	public enum ComputeType {
		//加减乘除
		INCREASE("+"), DECREASE("-"), MULTIPLY("*"), DIVIDE("/");

		private final String value;

		ComputeType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}	
	}

	// 响应类型
	public enum ContentType {
		
		TEXT("text/plain"), JSON("application/json"), XML("text/xml"), HTML("text/html"), JAVASCRIPT("text/javascript");

		private final String value;

		ContentType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
	
	// 编码
	public enum CodeType {

		UTF8("UTF-8"),GBK("GBK"),GB2312("GB2312"),ISO88591("ISO-8859-1"),GB18030("GB18030"), UTF16("UTF16");

		private final String value;

		CodeType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

	}
	
	// 存储方式
	public enum StorageType {

		YEAR {
			public int getValue() {
				return 0;
			}
		},
		MONTH {
			public int getValue() {
				return 1;
			}
		},
		DAY {
			public int getValue() {
				return 2;
			}
		},
		HOUR {
			public int getValue() {
				return 3;
			}
		};

		public abstract int getValue();
	}
}
