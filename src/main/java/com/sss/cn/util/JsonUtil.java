package com.sss.cn.util;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.annotation.AnnotationUtils;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

/**
 * json工具类
 * 
 * @author wangzh
 * 2017年12月19日 上午10:23:29
 */
public final class JsonUtil {
	private static final Log log = LogFactory.getLog(JsonUtil.class);
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	static{
		objectMapper.setSerializationInclusion(Include.NON_NULL); 
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setFilters(new SimpleFilterProvider().setFailOnUnknownId(false));
	}
	
	public static String toJson(Object value){
	    try {
	    	String jsonResult = objectMapper.writeValueAsString(value);
//	    	log.info("Json format: "+ jsonResult);
			return jsonResult;
		} catch (JsonProcessingException e) {
			log.error(e);
		}
		return null;
	}
	
	public static String toJsonInclude(Object value, String... properties){
		try {
			String jsonResult = objectMapper.writer(
			        new SimpleFilterProvider()
			        .addFilter(AnnotationUtils.getValue(AnnotationUtils.findAnnotation(value.getClass(), JsonFilter.class)).toString(), SimpleBeanPropertyFilter.filterOutAllExcept(properties)))
			        .writeValueAsString(value);
	    	log.info("Json format: "+ jsonResult);
			return jsonResult;
		} catch (JsonProcessingException e) {
			log.error(e);
		}
		return null;
	}
	
	public static String toJsonExclude(Object value, String... properties){
		try {
			String jsonResult = objectMapper.writer(
			        new SimpleFilterProvider()
			        .addFilter(AnnotationUtils.getValue(AnnotationUtils.findAnnotation(value.getClass(), JsonFilter.class)).toString(), SimpleBeanPropertyFilter.serializeAllExcept(properties)))
			        .writeValueAsString(value);
	    	log.info("Json format: "+ jsonResult);
			return jsonResult;
		} catch (JsonProcessingException e) {
			log.error(e);
		}
		return null;
	}
	
	public static <T> T fromJson(String json, Class<T> cls){  
        if(StringUtil.isEmpty(json)){  
            return null;      
        }  
        try {  
            return objectMapper.readValue(json, cls);  
        } catch (Exception e) {  
        	log.error(e);
        }  
        return null;  
    }  
	public static <T> List<T> fromJsonArray(String json){  
        if(StringUtil.isEmpty(json)){  
            return null;      
        }  
        try {  
        	return  objectMapper.readValue(json, new TypeReference<List<T>>(){});  
        } catch (Exception e) {  
        	log.error(e);
        }  
        return null;  
    } 
}
