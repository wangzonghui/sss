package com.sss.cn.util;

import java.text.DecimalFormat;
import java.util.UUID;

/**
 * UUID 获取
 *
 * @author Wangzh
 * @time:2016年7月21日 下午12:19:13
 */
public class UUIDUtil {

	/**
	 * 获取12位id
	 */
	public static String getId(){
		UUID uuid = UUID.randomUUID();
        int hashCode = uuid.toString().getBytes().hashCode();
        DecimalFormat df=new DecimalFormat("000000000000");
        String result=df.format(hashCode).replaceFirst("0", "1");
        return result;
	}
}
