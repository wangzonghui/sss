package com.sss.cn.New.model;

/**
 * 新闻信息类
 * @author wangzh 
 * @date 2016-07-20
 */ 

public class WhNew{

	/**
	 * 主键
	 */
	private String whNewId;
	/**
	 * 新闻标题
	 */
	private String whNewTitle;
	/**
	 * 新闻内容
	 */
	private String whNewContext;
	/**
	 * 原网页地址
	 */
	private String whNewUrl;
	/**
	 * 创建时间
	 */
	private long createTime;

	
	public String getWhNewUrl() {
		return whNewUrl;
	}

	public void setWhNewUrl(String whNewUrl) {
		this.whNewUrl = whNewUrl;
	}

	public void setWhNewId(String whNewId){
		this.whNewId=whNewId;
	}

	public String getWhNewId(){
		return whNewId;
	}

	public void setWhNewTitle(String whNewTitle){
		this.whNewTitle=whNewTitle;
	}

	public String getWhNewTitle(){
		return whNewTitle;
	}

	public void setWhNewContext(String whNewContext){
		this.whNewContext=whNewContext;
	}

	public String getWhNewContext(){
		return whNewContext;
	}

	public void setCreateTime(long createTime){
		this.createTime=createTime;
	}

	public long getCreateTime(){
		return createTime;
	}

}

