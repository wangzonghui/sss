package com.sss.cn.New.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;
import org.springframework.web.servlet.ModelAndView;

import com.sss.cn.New.model.WhNew;
import com.sss.cn.New.service.WhNewService;
import com.sss.cn.util.Page;
import com.sss.cn.util.ServiceResult;

/**
 * 新闻信息
 *
 * @author Wangzh
 * @time:2016年7月20日 下午4:23:01
 */
@Controller
@RequestMapping("/new")
public class WhNewAction {

	private static final Log log = LogFactory.getLog(WhNewAction.class);

	@Autowired
	private WhNewService whService;

	@RequestMapping("/test")
	public void getTest(String name) {
		log.info(name);
	}

	@RequestMapping("/{whNewId}")
	public ModelAndView getNewDetail(@PathVariable("whNewId") String whNewId) {
		System.out.println(whNewId);
		System.out.println(whService.getWhNew(whNewId).getWhNewContext());
		return new ModelAndView("newDetail", "whNew", whService.getWhNew(whNewId));
	}

	@ResponseBody
	@RequestMapping("/save")
	public ServiceResult<String> save(WhNew whNew) {
		return whService.saveWhNew(whNew);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/uploadExcel")
	public void uploadExcel(HttpServletRequest request, HttpServletResponse response) {
		System.out.println(request);
		MultipartFile multipartFile = ((MultipartRequest) request).getFile("file");
		String sourceName = multipartFile.getOriginalFilename(); // 原始文件名
		String fileType = sourceName.substring(sourceName.lastIndexOf("."));
		String name = request.getParameter("name");
		String pwd = request.getParameter("pwd");
		System.out.println(name);

		String base = request.getSession().getServletContext().getRealPath("/") + "attachments" + File.separator
				+ "uploadedExcel";
		File file = new File(base);
		if (!file.exists()) {
			file.mkdirs();
		}
		try {
			String path = base + File.separator + sourceName;

			multipartFile.transferTo(new File(path));
			// service.insert("insertAttachment", attach);
			// 上传成功后读取Excel表格里面的数据
			System.out.println("路径是" + path);

		} catch (Exception e) {
			try {
				response.getWriter().print("{success:false}");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	@RequestMapping("/page")
	public void getpage(){
		int pageSize=10;
		int pageNo=1;
		ServiceResult<Page> page=whService.getPage(pageNo, pageSize);
		System.out.println(page.getResult().getTotalCount());
		
	}
	
	/**
	 * 添加 request和response请求
	 * @param request
	 * @param response
	 */
	public void get(HttpServletRequest request,HttpServletResponse response){
		String context;
    	try {
			BufferedReader br = request.getReader();
			String str, wholeStr = "";
			while((str = br.readLine()) != null){
			wholeStr += str;
			}
			String name=(String) request.getParameter("name");
			context="你好";
			
			response.getOutputStream().write(context.getBytes());
			
		} catch (IOException e) {
			log.error(e,e);
		}
		
	}

}
