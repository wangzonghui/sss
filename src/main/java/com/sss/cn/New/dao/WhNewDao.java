package com.sss.cn.New.dao;

import com.sss.cn.New.model.WhNew;
import com.sss.cn.util.Page;

/**
 * 获取信息信息
 * 
 * @author wangzh
 */
public interface WhNewDao {
	/**
	 * 根据id获取新闻信息
	 */
	public WhNew getWhNew(String whNewId);
	
	/**
	 * 存储新闻信息
	 */
	public Boolean saveWhNew(WhNew whNew);

	/**
	 * 获取新闻列表
	 */
	public Page<WhNew> getPage(Page<WhNew> page);
}
