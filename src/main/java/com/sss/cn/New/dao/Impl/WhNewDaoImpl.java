package com.sss.cn.New.dao.Impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sss.cn.New.dao.WhNewDao;
import com.sss.cn.New.model.WhNew;
import com.sss.cn.util.DateUtil;
import com.sss.cn.util.JdbcHandler;
import com.sss.cn.util.Page;

@SuppressWarnings("all" )
@Repository
public class WhNewDaoImpl implements WhNewDao {

	@Autowired
	private JdbcHandler handler;
	
	private static final Log log = LogFactory.getLog(WhNewDaoImpl.class);
	
	/**
	 * 查询新闻信息
	 */
	public WhNew getWhNew(String whNewId) {
		String sql="select * from wh_new where wh_new_id="+whNewId;
		WhNew whNew=handler.getForObject(sql, WhNew.class, null);
		if(whNew!=null){
			return whNew;
		}
		return  null;
	}

	/**
	 * 存储新闻信息
	 */
	public Boolean saveWhNew(WhNew whNew) {
		String sql="insert into wh_new ( wh_new_id,wh_new_title,wh_new_url,wh_new_context,create_time) values(?,?,?,?,?)";
		
		List<Object> param= new ArrayList<Object>();
		param.add(whNew.getWhNewId());
		param.add(whNew.getWhNewTitle());
		param.add(whNew.getWhNewUrl());
		param.add(whNew.getWhNewContext());
		param.add(DateUtil.getTime());
		return handler.save(sql, param.toArray());
	}

	/**
	 * 获取新闻列表
	 */
	public Page<WhNew> getPage(Page<WhNew> page) {
		StringBuffer sql = new StringBuffer();
		
		sql.append("select * from wh_new ");
		
		List<Object> paramList = new ArrayList<Object>();
		
		
//		if(StringUtil.isNotBlank(status)){
//			sql.append(" and t.status= ?");
//			paramList.add(status);
//		}
		
		
		return handler.getForPage(sql.toString(), page, WhNew.class, paramList.toArray());
	}

}
