package com.sss.cn.New.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sss.cn.New.dao.WhNewDao;
import com.sss.cn.New.model.WhNew;
import com.sss.cn.New.service.WhNewService;
import com.sss.cn.util.Code;
import com.sss.cn.util.DateUtil;
import com.sss.cn.util.Page;
import com.sss.cn.util.ServiceResult;
import com.sss.cn.util.UUIDUtil;

@Service
public class WhNewServiceImpl implements WhNewService{

	@Autowired
	private WhNewDao whNewDao;
	
	/**
	 * 查询新闻信息
	 */
	public WhNew getWhNew(String whNewId) {
		return whNewDao.getWhNew(whNewId);
	}

	/**
	 * 保存新闻信息
	 */
	public ServiceResult<String> saveWhNew(WhNew whNew) {
		String id=UUIDUtil.getId();
		whNew.setWhNewId(id);
		whNew.setCreateTime(DateUtil.getTime());
		boolean result=whNewDao.saveWhNew(whNew);
		if(result){
			return new ServiceResult<String>(Code.SUCCESS,null,id);
		}
		return new ServiceResult<String>(Code.FAILED,null,null);
	}

	
	/**
	 * 获取新闻列表
	 */
	public ServiceResult<List<String>> getNewList() {
		
		return null;
	}

	@Override
	public ServiceResult<Page> getPage(Integer pageNo, Integer pageSize) {
		Page<WhNew> page = new Page<WhNew>(pageSize, pageNo);

		page = whNewDao.getPage(page);
		return new ServiceResult<Page>(Code.SUCCESS, "查询成功！",page);
	}

}
