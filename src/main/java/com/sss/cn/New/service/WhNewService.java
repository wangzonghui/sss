package com.sss.cn.New.service;

import java.util.List;

import com.sss.cn.New.model.WhNew;
import com.sss.cn.util.Page;
import com.sss.cn.util.ServiceResult;


public interface WhNewService {

	/**
	 * 根据id获取新闻信息
	 */
	public WhNew getWhNew(String whNewId);
	
	/**
	 * 存储信息信息
	 */
	public ServiceResult<String> saveWhNew(WhNew whNew);
	
	/**
	 * 获取新闻列表
	 */
	public ServiceResult<List<String>> getNewList();
	
	/**
	 * 获取新闻列表
	 */
	public ServiceResult<Page> getPage(Integer pageNo, Integer pageSize);
	
}
