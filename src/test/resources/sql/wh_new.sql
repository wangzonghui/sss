/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50626
Source Host           : 127.0.0.1:3306
Source Database       : wh_new_object

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2018-04-21 12:59:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wh_new
-- ----------------------------
DROP TABLE IF EXISTS `wh_new`;
CREATE TABLE `wh_new` (
  `wh_new_id` varchar(225) NOT NULL,
  `wh_new_title` varchar(225) NOT NULL DEFAULT '0' COMMENT '新闻标题',
  `wh_new_url` varchar(225) NOT NULL DEFAULT '0' COMMENT '原网页地址',
  `wh_new_context` text NOT NULL COMMENT '新闻内容',
  `create_time` varchar(14) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`wh_new_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新闻信息主表';

-- ----------------------------
-- Records of wh_new
-- ----------------------------
INSERT INTO `wh_new` VALUES ('1', '12', '012', '12', '012');
