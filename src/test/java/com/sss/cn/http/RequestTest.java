package com.sss.cn.http;

import com.sss.cn.util.HttpUtil;

/**
 * web请求测试类
 * 
 * @author wangzh
 * 2018年4月21日 下午12:06:11
 */
public class RequestTest {
	
	public static void main(String[] args) {
		
		String json="nihao";
		String url="http://localhost:8080/sss/new/1";  //http://localhost:8080/sss/new/test
		String result=HttpUtil.doHttpPost(json, url);
		System.out.println(result);
	}

}
