package com.sss.cn.junit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sss.cn.New.service.WhNewService;
import com.sss.cn.util.Page;
import com.sss.cn.util.ServiceResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/sss.xml")
public class NewServiceTest {
	
	private static final Log log = LogFactory.getLog(NewServiceTest.class);
	
	@Autowired
	private WhNewService whService;
	
	
	@Test
	public void getTest(){
		ServiceResult<Page> page=whService.getPage(1, 5);
		log.info(page.getResult().getTotalCount());
	}

}
